# Pipeline Jobs

Este repositório contém jobs que podem ser usados adicionalmente em outros repositórios com o parâmetro **include**.

Dois jobs estão disponíveis neste repositório:

| Job | Descrição | Links |
| --- | --- | --- |
| dependency check | Job baseado na tool Trivy, uma ferramenta para verificação de segurança no código do repositório | [Site Oficial](https://trivy.dev/) <br><br>[Repositório GitHub](https://github.com/aquasecurity/trivy) |
| sitespeed | Job baseado na tool Sitespeed, uma ferramenta para monitorar e melhorar o desempenho de websites | [Site Oficial](https://www.sitespeed.io/) <br><br> [Documentação](https://www.sitespeed.io/documentation/sitespeed.io/configuration/) |